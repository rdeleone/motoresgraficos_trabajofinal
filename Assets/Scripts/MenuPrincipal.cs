﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour
{

    public void PlayScene()
    {
        GetComponent<AudioSource>().Play();
        Invoke("Nuevojuego", GetComponent<AudioSource>().clip.length);
    }

    void Nuevojuego()
    {
        SceneManager.LoadScene("TrabajoFinal");
    }
       
    public void Quit()
    {
        GetComponent<AudioSource>().Play();
        Invoke("Salir", GetComponent<AudioSource>().clip.length);
    }
    void Salir()
    {
        Application.Quit();
    }
}
