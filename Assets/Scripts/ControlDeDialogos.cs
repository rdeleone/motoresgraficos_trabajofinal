﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeDialogos : MonoBehaviour
{
    public GameObject msgPanel;
    

    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<Collider>().isTrigger = true;
        msgPanel.SetActive(false);
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag ("NPC"))
        {
            msgPanel.SetActive(true);
            Debug.Log("daleee");
        }
                 
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("NPC"))
        {
            msgPanel.SetActive(false);
        }
    }
}
