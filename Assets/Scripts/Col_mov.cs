﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Col_mov : MonoBehaviour
{
    public float rapidez = 10f;
    private int cont;
    public CapsuleCollider col;
    private Rigidbody rb;
    public Text Estado;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cont = 0;
        col = GetComponent<CapsuleCollider>();
    }

    private void FixedUpdate()
    {
        float movhor = Input.GetAxis("Horizontal") * rapidez;
        float movver = Input.GetAxis("Vertical") * rapidez;


        movhor *= Time.deltaTime;
        movver *= Time.deltaTime;

        transform.Translate(movhor, 0, movver);

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Objeto") == true)
        {
            cont = cont + 1;
            //Debug.Log ("agarre el objeto");
            other.gameObject.SetActive(false);
            Estado.text = "¡Ganaste!";
            Time.timeScale = 0;
            
        }

    }
}
