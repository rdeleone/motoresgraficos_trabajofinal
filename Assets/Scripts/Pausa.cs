﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Pausa : MonoBehaviour
{

    bool Activar;
    Canvas MenuPausa;
    

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        MenuPausa = GetComponent<Canvas>();
        MenuPausa.enabled = false;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
          
           Activar = !Activar;
           MenuPausa.enabled = Activar;
           Time.timeScale = 0;
           
        
        }
           
         
    }

    public void Menu()
    {
        GetComponent<AudioSource>().Play();
        Invoke("MenuPrincipal", GetComponent<AudioSource>().clip.length);
    }

    public void Quit()
    {
        GetComponent<AudioSource>().Play();
        Invoke("Salir", GetComponent<AudioSource>().clip.length);
    }

    //Metodos de funciones

    void MenuPrincipal()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }
    void Salir()
    {
        Application.Quit();
    }
}
