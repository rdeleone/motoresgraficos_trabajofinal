﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class GestorPersistencia : MonoBehaviour
{
    public static GestorPersistencia instancia;
    public DataPersistencia data;
    string archivoDatos = "Guardado.dat";

    // Start is called before the first frame update
    void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);
        CargarDataPErsistencia();
    }

    private void CargarDataPErsistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
[System.Serializable]
public class DataPersistencia 
{
    public Punto posicionJugador;

}

[System.Serializable]
public class Punto
{
    public float x;
    public float y;
    public float z;

    public Punto(Vector3 p)
    {
        x = p.x;
        y = p.y;
        z = p.z;

    }
}